# Eclipse Local Repository Script

This script is designed to make replicating a local eclipse directory a little more forgiving. The idea is to configure up a directory to house the repository and then run the command when things need updating. 

## Setup 
A path to the Eclipse binary needs to be supplied in the script. Edit the file to make this change.

Intial setup is to run something like this:
``` 
mkdir repository
cd repository
p2-get.sh http://path-to-repository
```

The above command will:
1. Create a new `.p2-repository` file with the path to the repository stored in it. 
2. Download the artifacts and repository to this directory.

## Re-run
After initial configuration, just run the command `p2-get.sh` in the directory with the repository and all should be good.