#!/bin/bash

###########################################################
#                                                         #
#  Downloads an eclipse repository based on a             #
#   .p2-repository file and places them into a local      #
#   directory for consumption.                            #
#                                                         #
#  By Matt Jones narkie at gee may ell dot com            #
#                                                         #
###########################################################

# Configuration - set this to your eclipse runtime examples are:
# ECLIPSE_PATH="/Users/meme/Downloads/Eclipse.app/Contents/MacOS/Eclipse" 
ECLIPSE_PATH=""

## wrap in function
# Check out the input and set up a new repository
function link_process
{
    regex='(https?|ftp|file)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'
    if [[ $1 =~ $regex ]]
    then
        echo "$1 is a link storing this as a .p2-repository file"
        if [ ! -f $(pwd)/.p2-repository ]
        then
            echo $1 > .p2-repository
        else
            echo "Trying to overwrite existing config. Run without any args"
        fi
    fi
}

# Locate repository file.

function file_path_helper
{
	# testing if the current directory contains directive
	if [ -e $(pwd)/.p2-repository ]
	then
		echo "local directory contains p2-repository link"
		REPOSITORY_PATH=$(pwd)
		REPOSITORY="$(cat $REPOSITORY_PATH/.p2-repository)"
    fi

    # testing if the path to the directory containing the file was provided
    if [[ -e "$1/.p2-repository" && -d $1 ]]
    then
    	echo "Command line contains path to directory containing .p2-repository"
    	REPOSITORY_PATH=$1
    	REPOSITORY="$(cat $REPOSITORY_PATH/.p2-repository)"
	fi

	# test if the path to the .p2-repository file was provided
	if [[ $(echo basename $1)==".p2-repository" && -f $1 ]]
	then
	    echo "Found p2-repository on path"
	    REPOSITORY_PATH=$(dirname $1)
	    REPOSITORY="$(cat $1)"
	fi
}

link_process $1
file_path_helper $1

echo "Using Eclipse Path: $ECLIPSE_PATH"
echo "Using Repository Path: $REPOSITORY_PATH"
echo "Downloading from: $REPOSITORY"


$($ECLIPSE_PATH -nosplash -application org.eclipse.equinox.p2.metadata.repository.mirrorApplication -source $REPOSITORY -destination $REPOSITORY_PATH) &
$($ECLIPSE_PATH -nosplash -application org.eclipse.equinox.p2.artifact.repository.mirrorApplication -source $REPOSITORY -destination $REPOSITORY_PATH) &